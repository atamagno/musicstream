package com.weezzler.android;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weezzler.android.model.Album;
import com.weezzler.android.model.Artist;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;
import com.weezzler.android.server.ServerManager;
import com.weezzler.android.ui.AlbumFragment;
import com.weezzler.android.ui.ArtistFragment;
import com.weezzler.android.ui.MediaTypeFragment;
import com.weezzler.android.ui.PlayQueueFragment;
import com.weezzler.android.ui.RemoteControlsFragment;
import com.weezzler.android.ui.SearchResultAdapter;
import com.weezzler.android.ui.VolumeControlFragment;
import com.weezzler.android.utils.CodeGenerator;
import com.weezzler.android.utils.NetworkHelper;

import java.io.IOException;

public class LibraryActivity extends AppCompatActivity implements MediaTypeFragment.LibraryFragmentListener {

    private final static String volumeControlTag = "com.weezzler.android.VolumeControlTag";

    private final static int READ_EXTERNAL_STORAGE_PERMISSION = 1;

    private boolean permissionGranted = false;

    private static boolean isPaused;

    private PowerManager powerManager;

    private RelativeLayout mPanelInfo;
    private RelativeLayout mPanelMediaTypeSelection;
    private TextView mTextViewHelp;
    private TextView mTextViewCode;
    private Button mNewCodeButton;
    private FloatingActionButton mShuffleButton;
    private FrameLayout mLibraryContainer;
    private ListView mResultsListView;
    private SearchView mSearchView;
    private MenuItem mSwitchMediaTypeMenuItem;
    private MenuItem mSearchMenuItem;
    private MenuItem mPlayQueueMenuItem;
    private MenuItem mVolumeMenuItem;
    private CardView mControlsContainer;

    private String wsUrl;

    private RemoteControlsFragment mControlsFragment;
    private PlayQueueFragment mPlayQueueFragment;

    private static WifiManager.WifiLock wifiLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        if (wifiLock == null) {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF , "weezzlerWifiLock");

            if (!wifiLock.isHeld()){
                wifiLock.acquire();
            }
        }

        if (MediaLibrary.jsonMusicLibrary == null) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_PERMISSION);
            } else {
                permissionGranted = true;
                ServerManager.init(this);
            }
        } else {
            permissionGranted = true;
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.library_container, new MediaTypeFragment())
                    .commit();
        }

        ServerManager.setCurrentActivity(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                shouldDisplayHomeUp();
            }
        });

        initControls();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                    ServerManager.init(this);
                    setVisibleControls();
                } else {
                    permissionGranted = false;
                }
            }
        }
    }

    private void initControls() {

        mPlayQueueFragment = new PlayQueueFragment();

        android.app.FragmentManager fm = getFragmentManager();
        mControlsFragment = (RemoteControlsFragment) fm.findFragmentById(R.id.fragment_remote_controls);
        fm.beginTransaction().show(mControlsFragment).commit();

        mControlsContainer = (CardView) findViewById(R.id.controls_container);
        mControlsContainer.setVisibility(View.GONE);

        mLibraryContainer = (FrameLayout) findViewById(R.id.library_container);
        mResultsListView = (ListView) findViewById(R.id.results_list_view);

        mPanelInfo = (RelativeLayout) findViewById(R.id.panelInfo);
        mPanelMediaTypeSelection = (RelativeLayout) findViewById(R.id.panelMediaTypeSelection);
        mTextViewHelp = (TextView) findViewById(R.id.textViewHelp);
        mTextViewCode = (TextView) findViewById(R.id.textViewCode);
        mNewCodeButton = (Button) findViewById(R.id.newCodeButton);
        mNewCodeButton.setText(getString(R.string.get_new_code));

        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
    }

    public void showLibrary() {

        if (MediaLibrary.mediaType != null) {
            mPanelMediaTypeSelection.setVisibility(View.GONE);

            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            boolean canGoBack = backStackEntryCount > 0;

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(canGoBack);
            }

            if (!canGoBack) {
                if (mSwitchMediaTypeMenuItem != null) mSwitchMediaTypeMenuItem.setVisible(true);
                if (mSearchMenuItem != null) mSearchMenuItem.setVisible(true);
                if (mPlayQueueMenuItem != null) mPlayQueueMenuItem.setVisible(true);
                if (mVolumeMenuItem != null) mVolumeMenuItem.setVisible(true);
            }

            mPanelInfo.setVisibility(View.GONE);
            mLibraryContainer.setVisibility(View.VISIBLE);

            initShuffleButton();

        } else {
            mPanelMediaTypeSelection.setVisibility(View.VISIBLE);
        }
    }

    public void showMediaSelectionPanel() {

        if (mLibraryContainer.getVisibility() != View.VISIBLE) {
            mPanelInfo.setVisibility(View.GONE);
            mPanelMediaTypeSelection.setVisibility(View.VISIBLE);
        }
    }

    private void initShuffleButton() {
        mShuffleButton = (FloatingActionButton) findViewById(R.id.shuffle_button);
        mShuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ServerManager.sendShuffleAll();
            Context context = getApplicationContext();
            String toastText =  MediaLibrary.isVideoSelected() ? getString(R.string.shuffle_all_videos) : getString(R.string.shuffle_all_music);
            Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 140);
            toast.show();
            }
        });

        shouldShowShuffleButton();
    }

    public void showToastNotConnected() {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, getString(R.string.disconnected_msg), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 140);
        toast.show();
    }

    private void shouldShowShuffleButton() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 0) {

            mShuffleButton.setVisibility(View.VISIBLE);
        }
    }

    private void hideRemoteControls() {
        mControlsContainer.setVisibility(View.GONE);
    }

    private void showRemoteControls() {

        if (powerManager != null && powerManager.isScreenOn() && !isPaused) {
            if (MediaLibrary.selectedPlayableItem != null) {
                mControlsFragment.updateViewInfo();
                mControlsContainer.setVisibility(View.VISIBLE);
            } else {
                hideRemoteControls();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        isPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        isPaused = false;

        showRemoteControls();
    }

    private void initSearchList() {

        mSwitchMediaTypeMenuItem.setVisible(false);
        mPlayQueueMenuItem.setVisible(false);
        mVolumeMenuItem.setVisible(false);

        mLibraryContainer.setVisibility(View.GONE);
        mShuffleButton.setVisibility(View.GONE);
        mResultsListView.setVisibility(View.VISIBLE);
        hideRemoteControls();

        String searchLabel;
        switch (MediaLibrary.currentView) {
            case MediaTypeFragment.ARTISTS_VIEW:
                mSearchView.setQueryHint(getString(R.string.search_artists));
                break;
            case MediaTypeFragment.ALBUMS_VIEW:
                searchLabel = MediaLibrary.isVideoSelected() ? getString(R.string.search_folders) : getString(R.string.search_albums);
                mSearchView.setQueryHint(searchLabel);
                break;
            case MediaTypeFragment.SONGS_VIEW:
                searchLabel = MediaLibrary.isVideoSelected() ? getString(R.string.search_videos) : getString(R.string.search_songs);
                mSearchView.setQueryHint(searchLabel);
                break;
        }

        SearchResultAdapter mSearchResultAdapter = new SearchResultAdapter(LibraryActivity.this);
        mResultsListView.setAdapter(mSearchResultAdapter);
        mResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LibraryItem item = (LibraryItem) mResultsListView.getItemAtPosition(position);
                navigateToChildren(item);
            }
        });
    }

    private void backFromSearch() {

        mLibraryContainer.setVisibility(View.VISIBLE);
        mResultsListView.setVisibility(View.GONE);

        if (mSearchMenuItem.isVisible()) {
            shouldShowShuffleButton();
            showRemoteControls();
            mSwitchMediaTypeMenuItem.setVisible(true);
            mPlayQueueMenuItem.setVisible(true);
            mVolumeMenuItem.setVisible(true);
        }
    }

    private void setVisibleControls() {

        if (!MediaLibrary.musicLibrarySent) {

            if (!permissionGranted) {

                mTextViewHelp.setText(R.string.permission_needed);
                mTextViewCode.setVisibility(View.GONE);
                mNewCodeButton.setVisibility(View.GONE);

            } else {

                if (NetworkHelper.isOnline(this)) {

                    wsUrl = NetworkHelper.getIpAddress() + ":8887";

                    if (CodeGenerator.code == null) {
                        getConnectionCode(this);
                    } else {

                        String helpText = getHelpText();
                        mTextViewHelp.setText(Html.fromHtml(helpText));
                        mTextViewCode.setText(CodeGenerator.code);
                        mNewCodeButton.setVisibility(View.VISIBLE);
                    }

                } else {
                    mTextViewCode.setVisibility(View.GONE);
                    mTextViewHelp.setText(getString(R.string.no_network_error));
                    mNewCodeButton.setVisibility(View.VISIBLE);
                    mNewCodeButton.setText(R.string.try_again);
                }
            }

        } else {
            showLibrary();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options_menu, menu);
        mSwitchMediaTypeMenuItem = menu.findItem(R.id.switch_media_type);
        mSearchMenuItem = menu.findItem(R.id.menu_search);
        mPlayQueueMenuItem = menu.findItem(R.id.action_queue);
        mVolumeMenuItem = menu.findItem(R.id.action_volume);

        if (MediaLibrary.mediaType != null && MediaLibrary.mediaType.equals("video")) {
            mSwitchMediaTypeMenuItem.setIcon(R.drawable.ic_library_music_white_24dp);
        } else {
            mSwitchMediaTypeMenuItem.setIcon(R.drawable.ic_video_library_white_24dp);
        }

        setVisibleControls();

        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {

                        initSearchList();
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {

                        backFromSearch();
                        return true;
                    }
                });

        mSearchView = (SearchView) mSearchMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String queryText) {

                ListView listView = (ListView) findViewById(R.id.results_list_view);
                if (listView != null) {
                    SearchResultAdapter adapter = (SearchResultAdapter) listView.getAdapter();
                    adapter.getFilter().filter(queryText);
                }

                return false;
            }
        });

        return true;
    }

    private String getHelpText() {
        return getString(R.string.help_text_start) + " <b>" + getString(R.string.web_site) + "</b> " + getString(R.string.help_text_end);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_queue) {

            if (!MediaLibrary.updatingPlayQueue) {
                mPlayQueueMenuItem.setVisible(false);
                initFragment(mPlayQueueFragment);
            } else {
                Context context = getApplicationContext();
                Toast toast = Toast.makeText(context, getString(R.string.play_queue_updating), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM, 0, 140);
                toast.show();
            }
        } else if (id == R.id.action_volume) {
            VolumeControlFragment volumeControlFragment = new VolumeControlFragment();
            volumeControlFragment.show(getSupportFragmentManager(), volumeControlTag);
        } else if (id == R.id.switch_media_type) {
            MediaLibrary.mediaType = MediaLibrary.isVideoSelected() ? "audio" : "video";
            refreshView(false);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    private void shouldDisplayHomeUp() {

        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        boolean canGoBack = backStackEntryCount > 0;

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(canGoBack);
        }

        if (!canGoBack) {
            mSwitchMediaTypeMenuItem.setVisible(true);
            mSearchMenuItem.setVisible(true);
            mPlayQueueMenuItem.setVisible(true);
            mVolumeMenuItem.setVisible(true);
            mShuffleButton.setVisibility(View.VISIBLE);
        }
    }

    public void onMediaItemSelected(LibraryItem item) {
        navigateToChildren(item);
    }

    private void navigateToChildren(LibraryItem item) {

        switch (item.getType()) {
            case ARTIST:
                MediaLibrary.selectedArtist = (Artist) item;
                initFragment(new ArtistFragment());
                break;
            case ALBUM:
                MediaLibrary.selectedAlbum = (Album) item;
                initFragment(new AlbumFragment());
                break;
            case SONG:
                playSong(item, "all");
                break;
        }
    }

    public void playSong(LibraryItem item, String viewName) {

        if (MediaLibrary.selectedPlayableItem != null) {
            MediaLibrary.selectedPlayableItem.setIsPlaying(false);
        }

        MediaLibrary.selectedPlayableItem = (PlayableItem) item;
        MediaLibrary.selectedPlayableItem.setIsPlaying(true);
        ServerManager.sendPlaySong((PlayableItem) item, viewName);
        mControlsFragment.setSong();
        updateSelectedSongInQueue();
        showRemoteControls();
    }

    private void initFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.library_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        mShuffleButton.setVisibility(View.GONE);
        mSwitchMediaTypeMenuItem.setVisible(false);
        mSearchMenuItem.setVisible(false);
        mSearchMenuItem.collapseActionView();
    }

    public void setPlayingSong(String songId, int playingPosition) {
        PlayableItem playableItem = MediaLibrary.getPlayableItemById(songId);
        if (playableItem != null) {

            if (MediaLibrary.selectedPlayableItem != null) {
                MediaLibrary.selectedPlayableItem.setIsPlaying(false);
            }

            MediaLibrary.playingPosition = playingPosition;

            MediaLibrary.selectedPlayableItem = playableItem;
            MediaLibrary.selectedPlayableItem.setIsPlaying(true);
            mControlsFragment.setSong();
            updateSelectedSongInQueue();
            showRemoteControls();
        }
    }

    private void updateSelectedSongInQueue() {
        mPlayQueueFragment.updateSelectedSong();
    }

    public void updatePlayQueue() {
        mPlayQueueFragment.updatePlayQueue();
    }

    public void refreshView(boolean fromBrowser) {

        if (!fromBrowser) {
            ServerManager.sendMediaType(MediaLibrary.mediaType);
        }

        if (MediaLibrary.mediaType.equals("video")) {
            MediaLibrary.currentView = MediaTypeFragment.ALBUMS_VIEW;
            mSwitchMediaTypeMenuItem.setIcon(R.drawable.ic_library_music_white_24dp);
        } else {
            MediaLibrary.currentView = MediaTypeFragment.ARTISTS_VIEW;
            mSwitchMediaTypeMenuItem.setIcon(R.drawable.ic_video_library_white_24dp);
        }

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.library_container);
        if (currentFragment != null && currentFragment instanceof MediaTypeFragment) {
            ((MediaTypeFragment)currentFragment).initViewPager();
        }

        MediaLibrary.selectedPlayableItem = null;

        ServerManager.sendClearPlayQueue();

        hideRemoteControls();
    }

    public void selectAudio(View view) {
        MediaLibrary.mediaType = "audio";
        refreshView(false);
        showLibrary();
    }

    public void selectVideo(View view) {
        MediaLibrary.mediaType = "video";
        refreshView(false);
        showLibrary();
    }

    public void setPlayPause(boolean isPlaying) {
        mControlsFragment.setPlayPause(isPlaying);
    }

    public void getNewCode(View view) {
        getConnectionCode(this);
    }

    private void getConnectionCode(Context context) {
        if (NetworkHelper.isOnline(context)) {

            wsUrl = NetworkHelper.getIpAddress() + ":8887";

            mTextViewCode.setVisibility(View.GONE);
            mNewCodeButton.setVisibility(View.GONE);

            mTextViewHelp.setText(getString(R.string.get_error_wait));

            new GetCodeTask().execute();
        } else {
            mTextViewCode.setVisibility(View.GONE);
            mTextViewHelp.setText(getString(R.string.no_network_error));
            mNewCodeButton.setVisibility(View.VISIBLE);
            mNewCodeButton.setText(getString(R.string.try_again));
        }
    }

    private class GetCodeTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            try {
                return CodeGenerator.getCode(wsUrl);
            } catch (IOException e) {
                return getString(R.string.get_code_error);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != "" && result.length() == 6) {
                String helpText = getHelpText();
                mTextViewHelp.setText(Html.fromHtml(helpText));
                mTextViewCode.setText(result);

                mTextViewCode.setVisibility(View.VISIBLE);

                CodeGenerator.code = result;
            } else {
                mTextViewHelp.setText(getString(R.string.get_code_error));
            }

            mNewCodeButton.setVisibility(View.VISIBLE);
            mNewCodeButton.setText(getString(R.string.get_new_code));
        }
    }
}
