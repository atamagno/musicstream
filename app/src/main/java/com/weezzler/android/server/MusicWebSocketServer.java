package com.weezzler.android.server;

import android.os.Handler;
import android.os.Looper;

import com.weezzler.android.LibraryActivity;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;

class MusicWebSocketServer extends WebSocketServer {

	private LibraryActivity mLibraryActivity;
	private boolean connected;

	@Override
	public void onStart() {
		setConnectionLostTimeout(10);
	}

	public MusicWebSocketServer(int port) {
		super(new InetSocketAddress(port));
	}

	public void setCurrentActivity(LibraryActivity libraryActivity) {
		mLibraryActivity = libraryActivity;
	}

	private static void runOnUiThread(Runnable runnable){
		final Handler UIHandler = new Handler(Looper.getMainLooper());
		UIHandler.post(runnable);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {

		this.connected = true;
		this.sendMusicLibrary();
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		this.connected = false;
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		this.connected = false;
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		try {
			JSONObject jsonMessage = new JSONObject(message);
			String type = jsonMessage.getString("type");

			final int playingPosition;
			switch (type) {
				case "libraryLoaded":
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.showMediaSelectionPanel();
							}
						}
					});
					break;
				case "song":
					final String songId = jsonMessage.getString("id");
					playingPosition = jsonMessage.getInt("playingPosition");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.setPlayingSong(songId, playingPosition);
							}
						}
					});
					break;
				case "playPause":
					final boolean isPlaying = jsonMessage.getBoolean("isPlaying");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.setPlayPause(isPlaying);
							}
						}
					});
					break;
				case "addToPlayQueue":
					playingPosition = jsonMessage.getInt("playingPosition");
					MediaLibrary.setPlayQueue(jsonMessage, playingPosition);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.updatePlayQueue();
							}
						}
					});
					break;
				case "clearPlayQueue":
					MediaLibrary.playQueue = new ArrayList<>();
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.updatePlayQueue();
							}
						}
					});
					break;
				case "volume":
					MediaLibrary.volume = jsonMessage.getInt("volume");
					break;
				case "mediaType":
					MediaLibrary.mediaType = jsonMessage.getString("mediaType");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mLibraryActivity != null) {
								mLibraryActivity.refreshView(true);
								mLibraryActivity.showLibrary();
							}
						}
					});

					break;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void sendPlay() {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "play");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendPlaySong(PlayableItem playableItem, String viewName) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "playSong");
			jsonMessage.put("songId", playableItem.getId());
			jsonMessage.put("viewName", viewName);
			jsonMessage.put("songIndex", playableItem.getListPosition());
			jsonMessage.put("albumId", playableItem.getAlbum().getId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendPlayArtist(LibraryItem item) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "playArtist");
			jsonMessage.put("artistId", item.getId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendPlayAlbum(LibraryItem item) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "playAlbum");
			jsonMessage.put("albumId", item.getId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendPrevious() {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "previous");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendNext() {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "next");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendShuffleAll() {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "shuffleAll");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendShuffleArtist(LibraryItem item) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "shuffleArtist");
			jsonMessage.put("artistId", item.getId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendShuffleAlbum(LibraryItem item) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "shuffleAlbum");
			jsonMessage.put("albumId", item.getId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendAddToPlayQueue(ArrayList<PlayableItem> playableItemList) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		JSONArray jsonSongList = new JSONArray();
		JSONObject jsonSong;

		try {
			jsonMessage.put("type", "addToPlayQueue");
			for (PlayableItem playableItem : playableItemList) {
				jsonSong = new JSONObject();
				jsonSong.put("Id", playableItem.getId());
				jsonSong.put("Name", playableItem.getName());
				jsonSong.put("Duration", playableItem.getDuration());
				jsonSong.put("ArtistName", playableItem.getArtistName());
				jsonSongList.put(jsonSong);
			}
			jsonMessage.put("songList", jsonSongList);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendClearPlayQueue() {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		MediaLibrary.playQueue = new ArrayList<>();
		mLibraryActivity.updatePlayQueue();

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "clearPlayQueue");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	public void sendVolume(int volume) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		MediaLibrary.volume = volume;
		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "volume");
			jsonMessage.put("volume", volume);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	private void sendMusicLibrary() {
		if (MediaLibrary.jsonMusicLibrary != null)
		{
			MediaLibrary.musicLibrarySent = true;

			try {
				if (MediaLibrary.mediaType != null) {
					MediaLibrary.jsonMusicLibrary.put("mediaType", MediaLibrary.mediaType);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			this.sendToAll(MediaLibrary.jsonMusicLibrary.toString());
		}
	}

	public void sendMediaType(String mediaType) {

		if (!connected) {
			mLibraryActivity.showToastNotConnected();
			return;
		}

		JSONObject jsonMessage = new JSONObject();
		try {
			jsonMessage.put("type", "mediaType");
			jsonMessage.put("mediaType", mediaType);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.sendToAll(jsonMessage.toString());
	}

	private void sendToAll(String text) {
		Collection<WebSocket> con = connections();
		synchronized (con) {
			for(WebSocket c : con) {
				c.send(text);
			}
		}
	}
}
