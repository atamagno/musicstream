package com.weezzler.android.server;

import com.weezzler.android.LibraryActivity;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;

import java.io.IOException;
import java.util.ArrayList;

public class ServerManager {

    private static MusicWebSocketServer webSocketServer;
    private static MusicFileServer musicFileServer;

    public static void init(LibraryActivity mLibraryActivity) {

        if (MediaLibrary.jsonMusicLibrary == null) {
            MediaLibrary.getMusicLibrary(mLibraryActivity);
        }

        if (musicFileServer == null) {
            initMusicFileServer();
        }

        if (webSocketServer == null) {
            initWebSocketServer();
        }
    }

    public static void setCurrentActivity(LibraryActivity libraryActivity) {
        if (webSocketServer != null) {
            webSocketServer.setCurrentActivity(libraryActivity);
        }
    }

    private static void initMusicFileServer() {

        try {
            musicFileServer = new MusicFileServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initWebSocketServer() {
        int port = 8887;
        webSocketServer = new MusicWebSocketServer(port);
        webSocketServer.start();
    }

    public static void sendPlaySong(PlayableItem playableItem, String viewName) {
        webSocketServer.sendPlaySong(playableItem, viewName);
    }

    public static void sendPlayArtist(LibraryItem item) {
        webSocketServer.sendPlayArtist(item);
    }

    public static void sendPlayAlbum(LibraryItem item) {
        webSocketServer.sendPlayAlbum(item);
    }

    public static void addToPlayList(LibraryItem item) {
        ArrayList<PlayableItem> playableItemList = MediaLibrary.getPlayableItems(item);
        webSocketServer.sendAddToPlayQueue(playableItemList);
    }

    public static void sendClearPlayQueue() {
        webSocketServer.sendClearPlayQueue();
    }

    public static void sendPlay() {
        webSocketServer.sendPlay();
    }

    public static void sendPrevious() {
        webSocketServer.sendPrevious();
    }

    public static void sendNext() {
        webSocketServer.sendNext();
    }

    public static void sendShuffleAll() {
        webSocketServer.sendShuffleAll();
    }

    public static void sendShuffleArtist(LibraryItem item) {
        webSocketServer.sendShuffleArtist(item);
    }

    public static void sendShuffleAlbum(LibraryItem item) {
        webSocketServer.sendShuffleAlbum(item);
    }

    public static void sendVolume(int volume) {
        webSocketServer.sendVolume(volume);
    }

    public static void sendMediaType(String mediaType) {
        webSocketServer.sendMediaType(mediaType);
    }
}
