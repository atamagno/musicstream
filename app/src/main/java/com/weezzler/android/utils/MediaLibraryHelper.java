package com.weezzler.android.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import com.weezzler.android.model.Album;
import com.weezzler.android.model.Artist;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MediaLibraryHelper {

    public void fetchMusicLibrary(Context context) {

        buildMediaLibrary(context);
        sortMediaLibrary();
        mediaLibraryToJSON();
    }

    public void buildMediaLibrary(Context context) {
        buildMusicLibrary(context);
        buildVideoLibrary(context);
    }

    public void buildMusicLibrary(Context context) {

        MediaLibrary.musicArtists = new ArrayList<>();
        MediaLibrary.musicAlbums = new ArrayList<>();
        MediaLibrary.songs = new ArrayList<>();

        ContentResolver musicResolver = context.getContentResolver();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        if (musicCursor != null && musicCursor.moveToFirst()) {

            int idColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int artistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumIdColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int fileLocationColumn = musicCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            int durationColumn = musicCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);

            do {

                long songId = musicCursor.getLong(idColumn);
                String songName = musicCursor.getString(songColumn);
                String artistName = musicCursor.getString(artistColumn);
                String albumName = musicCursor.getString(albumIdColumn);
                String fileLocation = musicCursor.getString(fileLocationColumn);
                int duration = musicCursor.getInt(durationColumn);

                if (validatePlayableItemInfo(songId, songName, albumName, artistName, fileLocation, duration)) {
                    addSong(songId, songName, albumName, artistName, fileLocation, duration, context);
                }
            }

            while (musicCursor.moveToNext());
        }

        if (musicCursor != null) {
            musicCursor.close();
        }
    }

    public void buildVideoLibrary(Context context) {

        MediaLibrary.videoArtists = new ArrayList<>();
        MediaLibrary.videoFolders = new ArrayList<>();
        MediaLibrary.videos = new ArrayList<>();

        ContentResolver musicResolver = context.getContentResolver();
        Uri videoUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        Cursor videoCursor = musicResolver.query(videoUri, null, null, null, null);

        if (videoCursor != null && videoCursor.moveToFirst()) {

            int idColumn = videoCursor.getColumnIndex(MediaStore.Video.Media._ID);
            int songColumn = videoCursor.getColumnIndex(MediaStore.Video.Media.TITLE);
            int artistColumn = videoCursor.getColumnIndex(MediaStore.Video.Media.ARTIST);
            int albumIdColumn = videoCursor.getColumnIndex(MediaStore.Video.Media.ALBUM);
            int fileLocationColumn = videoCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            int durationColumn = videoCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);

            BitmapFactory.Options options = new BitmapFactory.Options();

            do {

                long videoId = videoCursor.getLong(idColumn);
                String songName = videoCursor.getString(songColumn);
                String artistName = videoCursor.getString(artistColumn);
                String albumName = videoCursor.getString(albumIdColumn);
                String fileLocation = videoCursor.getString(fileLocationColumn);
                int duration = videoCursor.getInt(durationColumn);

                Bitmap thumbnail = null;

                try {
                    thumbnail = MediaStore.Video.Thumbnails.getThumbnail(musicResolver, videoId, MediaStore.Video.Thumbnails.MICRO_KIND, options);
                } catch (OutOfMemoryError o) {
                }

                if (validatePlayableItemInfo(videoId, songName, albumName, artistName, fileLocation, duration)) {
                    addVideo(videoId, songName, albumName, artistName, fileLocation, duration, thumbnail, context);
                }
            }

            while (videoCursor.moveToNext());
        }

        if (videoCursor != null) {
            videoCursor.close();
        }
    }

    private boolean validatePlayableItemInfo(long id, String songName, String albumName, String artistName, String fileLocation, int duration) {

        return id > 0 && duration > 0 &&
                songName != null && !songName.isEmpty() &&
                albumName != null && !albumName.isEmpty() &&
                artistName != null && !artistName.isEmpty() &&
                fileLocation != null && !fileLocation.isEmpty();
    }

    private void sortMediaLibrary() {
        Collections.sort(MediaLibrary.musicArtists, new Comparator<Artist>() {
            @Override
            public int compare(Artist a1, Artist a2) {
                return a1.getName().compareTo(a2.getName());
            }
        });

        Collections.sort(MediaLibrary.videoArtists, new Comparator<Artist>() {
            @Override
            public int compare(Artist a1, Artist a2) {
                return a1.getName().compareTo(a2.getName());
            }
        });

        Collections.sort(MediaLibrary.musicAlbums, new Comparator<Album>() {
            @Override
            public int compare(Album a1, Album a2) {
                return a1.getName().compareTo(a2.getName());
            }
        });

        Collections.sort(MediaLibrary.videoFolders, new Comparator<Album>() {
            @Override
            public int compare(Album a1, Album a2) {
                return a1.getName().compareTo(a2.getName());
            }
        });

        Collections.sort(MediaLibrary.songs, new Comparator<PlayableItem>() {
            @Override
            public int compare(PlayableItem s1, PlayableItem s2) {
                return s1.getName().compareTo(s2.getName());
            }
        });

        Collections.sort(MediaLibrary.videos, new Comparator<PlayableItem>() {
            @Override
            public int compare(PlayableItem s1, PlayableItem s2) {
                return s1.getName().compareTo(s2.getName());
            }
        });
    }

    private Artist getArtistByName(String artistName, ArrayList<Artist> artists) {

        Artist artist = null;

        for (Artist a : artists) {
            if (a.getName().equals(artistName)) {
                artist = a;
                break;
            }
        }

        return artist;
    }

    private Album getAlbumByName(String albumName, ArrayList<Album> albums) {

        Album album = null;

        for (Album a : albums) {
            if (a.getName().equals(albumName)) {
                album = a;
                break;
            }
        }

        return album;
    }

    private PlayableItem addSong(long id, String songName, String albumName, String artistName, String fileLocation, int duration, Context context) {

        Album album = null;

        Artist artist = getArtistByName(artistName, MediaLibrary.musicArtists);
        if (artist != null) {
            album = getAlbumByName(albumName, artist.getAlbums());
        }
        else {
            artist = new Artist(id, artistName, context);
            MediaLibrary.musicArtists.add(artist);
        }

        if (album == null) {
            album = new Album(id, albumName, artistName, context);
            artist.addAlbum(album);
            MediaLibrary.musicAlbums.add(album);
        }

        PlayableItem playableItem = new PlayableItem(id, songName, album, artistName, fileLocation, duration, null, context);

        album.addPlayableItem(playableItem);
        MediaLibrary.songs.add(playableItem);

        return playableItem;
    }

    private PlayableItem addVideo(long id, String videoName, String albumName, String artistName, String fileLocation, int duration, Bitmap thumbnail, Context context) {

        Album album = null;

        Artist artist = getArtistByName(artistName, MediaLibrary.videoArtists);
        if (artist != null) {
            album = getAlbumByName(albumName, artist.getAlbums());
        }
        else {
            artist = new Artist(id, artistName, context);
            MediaLibrary.videoArtists.add(artist);
        }

        if (album == null) {
            album = new Album(id, albumName, artistName, context);
            artist.addAlbum(album);
            MediaLibrary.videoFolders.add(album);
        }

        PlayableItem playableItem = new PlayableItem(id, videoName, album, artistName, fileLocation, duration, thumbnail, context);

        album.addPlayableItem(playableItem);
        MediaLibrary.videos.add(playableItem);

        return playableItem;
    }

    public void mediaLibraryToJSON() {

        JSONObject jsonResponse = new JSONObject();

        musicLibraryToJSON(jsonResponse);
        videoLibraryToJSON(jsonResponse);

        MediaLibrary.jsonMusicLibrary = jsonResponse;
    }

    private void musicLibraryToJSON(JSONObject jsonResponse) {

        if (MediaLibrary.musicArtists.size() > 0) {

            JSONArray jsonMusicLibrary = new JSONArray();

            JSONObject jsonArtist;
            JSONArray jsonAlbums;
            JSONObject jsonAlbum;
            JSONArray jsonSongs;
            JSONObject jsonSong;

            try {

                // Process all artists.
                for (Artist artist : MediaLibrary.musicArtists) {

                    jsonArtist = new JSONObject();
                    jsonArtist.put("Id", artist.getId());
                    jsonArtist.put("Name", artist.getName());

                    jsonAlbums = new JSONArray();
                    for (Album album: artist.getAlbums()) {

                        jsonAlbum = new JSONObject();
                        jsonAlbum.put("Id", album.getId());
                        jsonAlbum.put("Name", album.getName());
                        jsonAlbum.put("ArtistName", album.getArtistName());

                        jsonSongs = new JSONArray();
                        for (PlayableItem playableItem : album.getPlayableItems()) {

                            jsonSong = new JSONObject();
                            jsonSong.put("Id", playableItem.getId());
                            jsonSong.put("Name", playableItem.getName());
                            jsonSong.put("Duration", playableItem.getDuration());
                            jsonSong.put("ArtistName", playableItem.getArtistName());
                            jsonSongs.put(jsonSong);
                        }

                        jsonAlbum.put("Songs", jsonSongs);
                        jsonAlbums.put(jsonAlbum);
                    }

                    jsonArtist.put("Albums", jsonAlbums);
                    jsonMusicLibrary.put(jsonArtist);
                }

                // Process all albums.
                jsonAlbums = new JSONArray();
                for (Album album: MediaLibrary.musicAlbums) {

                    jsonAlbum = new JSONObject();
                    jsonAlbum.put("Id", album.getId());
                    jsonAlbum.put("Name", album.getName());
                    jsonAlbum.put("ArtistName", album.getArtistName());

                    jsonSongs = new JSONArray();
                    for (PlayableItem playableItem : album.getPlayableItems()) {

                        jsonSong = new JSONObject();
                        jsonSong.put("Id", playableItem.getId());
                        jsonSong.put("Name", playableItem.getName());
                        jsonSong.put("Duration", playableItem.getDuration());
                        jsonSong.put("ArtistName", playableItem.getArtistName());
                        jsonSong.put("VideoThumbnail", playableItem.getBitmapString());
                        jsonSongs.put(jsonSong);
                    }

                    jsonAlbum.put("Songs", jsonSongs);
                    jsonAlbums.put(jsonAlbum);
                }

                // Process all songs.
                jsonSongs = new JSONArray();
                for (PlayableItem playableItem : MediaLibrary.songs) {

                    jsonSong = new JSONObject();
                    jsonSong.put("Id", playableItem.getId());
                    jsonSong.put("Name", playableItem.getName());
                    jsonSong.put("Duration", playableItem.getDuration());
                    jsonSong.put("ArtistName", playableItem.getArtistName());
                    jsonSong.put("VideoThumbnail", playableItem.getBitmapString());
                    jsonSongs.put(jsonSong);
                }

                if (!jsonResponse.has("type")) {
                    jsonResponse.put("type", "library");
                    jsonResponse.put("library", jsonMusicLibrary);
                }

                jsonResponse.put("allAlbums", jsonAlbums);
                jsonResponse.put("allSongs", jsonSongs);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void videoLibraryToJSON(JSONObject jsonResponse) {

        if (MediaLibrary.videoArtists.size() > 0) {

            JSONArray jsonMusicLibrary = new JSONArray();

            JSONObject jsonArtist;
            JSONArray jsonAlbums;
            JSONObject jsonAlbum;
            JSONArray jsonSongs;
            JSONObject jsonSong;

            try {

                // Process all artists.
                for (Artist artist : MediaLibrary.videoArtists) {

                    jsonArtist = new JSONObject();
                    jsonArtist.put("Id", artist.getId());
                    jsonArtist.put("Name", artist.getName());

                    jsonAlbums = new JSONArray();
                    for (Album album: artist.getAlbums()) {

                        jsonAlbum = new JSONObject();
                        jsonAlbum.put("Id", album.getId());
                        jsonAlbum.put("Name", album.getName());
                        jsonAlbum.put("ArtistName", album.getArtistName());

                        jsonSongs = new JSONArray();
                        for (PlayableItem playableItem : album.getPlayableItems()) {

                            jsonSong = new JSONObject();
                            jsonSong.put("Id", playableItem.getId());
                            jsonSong.put("Name", playableItem.getName());
                            jsonSong.put("Duration", playableItem.getDuration());
                            jsonSong.put("ArtistName", playableItem.getArtistName());
                            jsonSongs.put(jsonSong);
                        }

                        jsonAlbum.put("Songs", jsonSongs);
                        jsonAlbums.put(jsonAlbum);
                    }

                    jsonArtist.put("Albums", jsonAlbums);
                    jsonMusicLibrary.put(jsonArtist);
                }

                // Process all albums.
                jsonAlbums = new JSONArray();
                for (Album album: MediaLibrary.videoFolders) {

                    jsonAlbum = new JSONObject();
                    jsonAlbum.put("Id", album.getId());
                    jsonAlbum.put("Name", album.getName());
                    jsonAlbum.put("ArtistName", album.getArtistName());

                    jsonSongs = new JSONArray();
                    for (PlayableItem playableItem : album.getPlayableItems()) {

                        jsonSong = new JSONObject();
                        jsonSong.put("Id", playableItem.getId());
                        jsonSong.put("Name", playableItem.getName());
                        jsonSong.put("Duration", playableItem.getDuration());
                        jsonSong.put("ArtistName", playableItem.getArtistName());
                        jsonSong.put("VideoThumbnail", playableItem.getBitmapString());
                        jsonSongs.put(jsonSong);
                    }

                    jsonAlbum.put("Songs", jsonSongs);
                    jsonAlbums.put(jsonAlbum);
                }

                // Process all songs.
                jsonSongs = new JSONArray();
                for (PlayableItem playableItem : MediaLibrary.videos) {

                    jsonSong = new JSONObject();
                    jsonSong.put("Id", playableItem.getId());
                    jsonSong.put("Name", playableItem.getName());
                    jsonSong.put("Duration", playableItem.getDuration());
                    jsonSong.put("ArtistName", playableItem.getArtistName());
                    jsonSong.put("VideoThumbnail", playableItem.getBitmapString());
                    jsonSongs.put(jsonSong);
                }

                if (!jsonResponse.has("type")) {
                    jsonResponse.put("type", "library");
                }

                JSONObject jsonVideoLibrary = new JSONObject();
                jsonVideoLibrary.put("allArtists", jsonMusicLibrary);
                jsonVideoLibrary.put("allAlbums", jsonAlbums);
                jsonVideoLibrary.put("allVideos", jsonSongs);

                jsonResponse.put("videoLibrary", jsonVideoLibrary);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
