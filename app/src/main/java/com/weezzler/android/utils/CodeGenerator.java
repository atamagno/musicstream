package com.weezzler.android.utils;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CodeGenerator {

    public static String code;

    public static String getCode(String wsUrl) throws IOException {

        InputStream is = null;

        try {
            URL url = new URL("http://www.weezzler.com/connection/" + wsUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            conn.connect();
            is = conn.getInputStream();

            return readCode(is);

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private static String readCode(InputStream stream) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
        try {

            String code = "";
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("code")) {
                    code = reader.nextString();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();

            return code;
        } finally {
            reader.close();
        }
    }
}
