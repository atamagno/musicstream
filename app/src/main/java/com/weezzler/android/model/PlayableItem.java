package com.weezzler.android.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class PlayableItem extends LibraryItem {

    private final Album album;
    private final String artistName;
    private final String fileLocation;
    private final int duration;
    private boolean isPlaying;
    private Bitmap thumbnail;

    public PlayableItem(long id, String name, Album album, String artistName, String fileLocation, int duration, Bitmap thumbnail, Context context) {
        super(id, name, false, context);
        this.type = MediaType.SONG;
        this.album = album;
        this.artistName = artistName;
        this.fileLocation = fileLocation;
        this.duration = duration;
        this.thumbnail = thumbnail;
    }

    public Album getAlbum() {
        return album;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public String getArtistName() {
        return artistName;
    }

    public Bitmap getBitmap() {
        return thumbnail;
    }

    public String getBitmapString() {

        if (thumbnail != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b = baos.toByteArray();
            String temp = Base64.encodeToString(b, Base64.DEFAULT);
            return "data:image/png;base64," + temp;
        } else {
            return "";
        }
    }

    public String getDuration() {
        long minutes = (duration / 1000) / 60;
        int seconds = (duration / 1000) % 60;

        String strMinutes = Long.toString(minutes);
        strMinutes = strMinutes.length() == 1 ? "0" + strMinutes : strMinutes;

        String strSeconds = Integer.toString(seconds);
        strSeconds = strSeconds.length() == 1 ? "0" + strSeconds : strSeconds;

        return strMinutes + ":" + strSeconds;
    }

    public String getSubTitle(boolean isArtistView) {
        return MediaLibrary.isVideoSelected() ? album.getName() : artistName;
    }

    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public boolean IsPlaying() {
        return isPlaying;
    }
}
