package com.weezzler.android.model;

import android.content.Context;

public class LibraryItem {

    private long id;
    private String name;
    private String subTitle;
    private boolean isPlayable;
    MediaType type;
    private int listPosition = 0;
    Context context;

    public enum MediaType {
        ARTIST, ALBUM, SONG
    }

    LibraryItem() { }

    LibraryItem(long id, String name, boolean isPlayable, Context context) {
        this.id = id;
        this.name = name;
        this.isPlayable = isPlayable;
        this.context = context;
    }

    public Long getId() {
        return id;
    }

    public MediaType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getSubTitle(boolean isArtistView) {
        return subTitle;
    }

    public boolean isPlayable() {
        return isPlayable;
    }

    public void setListPosition(int position) {
        listPosition = position;
    }

    public int getListPosition() {
        return listPosition;
    }
}
