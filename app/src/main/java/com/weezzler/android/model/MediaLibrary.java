package com.weezzler.android.model;

import android.content.Context;

import com.weezzler.android.utils.MediaLibraryHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MediaLibrary {

    public static int currentView = 0;
    public static boolean isPlaying = false;
    public static String mediaType;

    public static ArrayList<Artist> musicArtists = null;
    public static ArrayList<Album> musicAlbums = null;
    public static ArrayList<PlayableItem> songs = null;

    public static ArrayList<Artist> videoArtists = null;
    public static ArrayList<Album> videoFolders = null;
    public static ArrayList<PlayableItem> videos = null;

    public static ArrayList<PlayableItem> playQueue = null;

    public static Artist selectedArtist = null;
    public static Album selectedAlbum = null;
    public static PlayableItem selectedPlayableItem = null;

    public static int playingPosition;

    public static JSONObject jsonMusicLibrary = null;
    public static boolean musicLibrarySent = false;
    public static boolean updatingPlayQueue = false;

    public static int volume = 100;

    public static ArrayList<Artist> getArtists() {
        return mediaType != null && mediaType.equals("video") ? videoArtists : musicArtists;
    }

    public static ArrayList<Album> getAlbums() {
        return mediaType != null && mediaType.equals("video") ? videoFolders : musicAlbums;
    }

    public static ArrayList<PlayableItem> getPlayableItems() {
        return mediaType != null && mediaType.equals("video") ? videos : songs;
    }

    public static void getMusicLibrary(Context context) {

        if (jsonMusicLibrary == null) {
            MediaLibraryHelper mediaLibraryHelper = new MediaLibraryHelper();
            mediaLibraryHelper.fetchMusicLibrary(context);
        }
    }

    public static Artist getArtistById(String id) {

        ArrayList<Artist> artists = getArtists();
        long longId = Long.parseLong(id);
        for (Artist artist : artists) {
            if (artist.getId() == longId)
            {
                return artist;
            }
        }

        return null;
    }

    public static Album getAlbumById(String id) {

        ArrayList<Album> albums = getAlbums();
        long longId = Long.parseLong(id);
        for (Album album : albums) {
            if (album.getId() == longId)
            {
                return album;
            }
        }

        return null;
    }

    public static PlayableItem getPlayableItemById(String id) {

        ArrayList<PlayableItem> playableItems = getPlayableItems();
        long longId = Long.parseLong(id);
        for (PlayableItem playableItem : playableItems) {
            if (playableItem.getId() == longId)
            {
                return playableItem;
            }
        }

        return null;
    }

    public static String getFileLocationById(String id) {

        ArrayList<PlayableItem> playableItems = getPlayableItems();
        long longId = Long.parseLong(id);
        for (PlayableItem playableItem : playableItems) {
            if (playableItem.getId() == longId)
            {
                return playableItem.getFileLocation();
            }
        }

        return "";
    }

    public static ArrayList<PlayableItem> getPlayableItems(LibraryItem item) {

        ArrayList<PlayableItem> playableItemList = new ArrayList<>();
        switch (item.getType()) {
            case ARTIST:
                playableItemList = getArtistPlayableItems((Artist)item);
                break;
            case ALBUM:
                playableItemList = getAlbumPlayableItems((Album)item);
                break;
            case SONG:
                playableItemList = new ArrayList<>();
                playableItemList.add((PlayableItem)item);
                break;
        }

        return playableItemList;
    }

    public static void setPlayQueue(JSONObject jsonMessage, int playingPosition) {

        try {

            updatingPlayQueue = true;

            JSONArray jsonPlayableItemArray = jsonMessage.getJSONArray("songIdList");

            playQueue = new ArrayList<>();

            for (int i = 0; i < jsonPlayableItemArray.length(); i++) {
                try {
                    Object playableItemId = jsonPlayableItemArray.get(i);
                    PlayableItem playableItem = getPlayableItemById(playableItemId.toString());
                    if (playableItem != null) {
                        playQueue.add(playableItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            MediaLibrary.playingPosition = playingPosition;

            updatingPlayQueue = false;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<PlayableItem> getArtistPlayableItems(Artist artist) {
        ArrayList<PlayableItem> playableItemList = new ArrayList<>();
        for (Album album : artist.getAlbums()) {
            for (PlayableItem playableItem : album.getPlayableItems()) {
                playableItemList.add(playableItem);
            }
        }

        return playableItemList;
    }

    private static ArrayList<PlayableItem> getAlbumPlayableItems(Album album) {
        ArrayList<PlayableItem> playableItemList = new ArrayList<>();
        for (PlayableItem playableItem : album.getPlayableItems()) {
            playableItemList.add(playableItem);
        }

        return playableItemList;
    }

    public static boolean isVideoSelected() {
        return mediaType != null && mediaType.equals("video");
    }
}
