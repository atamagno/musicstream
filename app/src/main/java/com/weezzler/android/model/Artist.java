package com.weezzler.android.model;

import android.content.Context;

import com.weezzler.android.R;

import java.util.ArrayList;

public class Artist extends LibraryItem {

    private final ArrayList<Album> albums;

    public Artist(long id, String name, Context context) {
        super(id, name, false, context);
        this.type = MediaType.ARTIST;
        this.albums = new ArrayList<>();
    }

    public void addAlbum(Album album) {
        albums.add(album);
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public String getSubTitle(boolean isArtistView) {

        String singularLabel = MediaLibrary.isVideoSelected() ? context.getString(R.string.folder_label_singular) : context.getString(R.string.album_label_singular);
        String pluralLabel = MediaLibrary.isVideoSelected() ? context.getString(R.string.folder_label_plural) : context.getString(R.string.album_label_plural);

        String albumLabel = albums.size() > 1 ? pluralLabel : singularLabel;

        return albums.size() + " " + albumLabel;
    }
}
