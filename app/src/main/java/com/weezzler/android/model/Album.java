package com.weezzler.android.model;

import android.content.Context;

import com.weezzler.android.R;

import java.util.ArrayList;

public class Album extends LibraryItem {

    private Long id;
    private final String artistName;
    private final ArrayList<PlayableItem> playableItems;

    public Album(long id, String name, String artistName, Context context) {
        super(id, name, false, context);
        this.type = MediaType.ALBUM;
        this.artistName = artistName;
        this.playableItems = new ArrayList<>();
    }

    public void addPlayableItem(PlayableItem playableItem) {
        playableItems.add(playableItem);
    }

    public String getArtistName() {
        return artistName;
    }

    public ArrayList<PlayableItem> getPlayableItems() {
        return playableItems;
    }

    public String getSubTitle(boolean isArtistView) {

        String singularLabel = MediaLibrary.isVideoSelected() ? context.getString(R.string.video_label_singular) : context.getString(R.string.song_label_singular);
        String pluralLabel = MediaLibrary.isVideoSelected() ? context.getString(R.string.video_label_plural) : context.getString(R.string.song_label_plural);

        String playableItemLabel = playableItems.size() > 1 ? pluralLabel : singularLabel;

        if (isArtistView) {
            return playableItems.size() + " " + playableItemLabel;
        } else {
            return artistName + " - " + playableItems.size() + " " + playableItemLabel;
        }
    }
}
