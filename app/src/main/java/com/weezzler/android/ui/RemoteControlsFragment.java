package com.weezzler.android.ui;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.weezzler.android.R;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.server.ServerManager;

public class RemoteControlsFragment extends Fragment {

    private TextView mTitle;
    private TextView mSubtitle;

    private ImageButton mButtonPlayPause;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_remote_controls, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        ImageButton mButtonPrevious = (ImageButton) view.findViewById(R.id.skip_previous);
        mButtonPrevious.setOnClickListener(mButtonPreviousListener);

        mButtonPlayPause = (ImageButton) view.findViewById(R.id.play_pause);
        mButtonPlayPause.setOnClickListener(mButtonPlayPauseListener);

        ImageButton mButtonNext = (ImageButton) view.findViewById(R.id.skip_next);
        mButtonNext.setOnClickListener(mButtonNextListener);

        mTitle = (TextView) view.findViewById(R.id.title);
        mSubtitle = (TextView) view.findViewById(R.id.artist);

        if (MediaLibrary.isPlaying) {
            setButtonImage(true);
        } else {
            setButtonImage(false);
        }

        if (MediaLibrary.selectedPlayableItem != null) {
            mTitle.setText(MediaLibrary.selectedPlayableItem.getName());
            mSubtitle.setText(MediaLibrary.selectedPlayableItem.getSubTitle(false));
        }
    }

    private void setButtonImage(boolean playingSong) {
        if (mButtonPlayPause != null) {
            Activity currentActivity = getActivity();
            if (currentActivity != null) {

                Drawable buttonImage;
                if (playingSong) {
                    buttonImage = ContextCompat.getDrawable(currentActivity, R.drawable.ic_pause_white_24dp);
                } else {
                    buttonImage = ContextCompat.getDrawable(currentActivity, R.drawable.ic_play_arrow_white_24dp);
                }

                if (buttonImage != null) {
                    mButtonPlayPause.setImageDrawable(buttonImage);
                }
            }
        }
    }

    public void setSong() {
        if (MediaLibrary.selectedPlayableItem != null) {
            mTitle.setText(MediaLibrary.selectedPlayableItem.getName());
            mSubtitle.setText(MediaLibrary.selectedPlayableItem.getSubTitle(false));
            setButtonImage(true);
            MediaLibrary.isPlaying = true;
        }
    }

    public void setPlayPause(boolean isPlaying) {

        if (mButtonPlayPause != null) {
            if (isPlaying) {
                setButtonImage(true);
            } else {
                setButtonImage(false);
            }
        }

        MediaLibrary.isPlaying = isPlaying;
    }

    public void updateViewInfo() {
        if (MediaLibrary.selectedPlayableItem != null) {
            mTitle.setText(MediaLibrary.selectedPlayableItem.getName());
            mSubtitle.setText(MediaLibrary.selectedPlayableItem.getSubTitle(false));
        }
    }

    private final View.OnClickListener mButtonPreviousListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ServerManager.sendPrevious();
        }
    };

    private final View.OnClickListener mButtonPlayPauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ServerManager.sendPlay();
            setPlayPause(MediaLibrary.isPlaying);
        }
    };

    private final View.OnClickListener mButtonNextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ServerManager.sendNext();
        }
    };
}
