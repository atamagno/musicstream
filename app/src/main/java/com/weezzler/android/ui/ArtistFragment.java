package com.weezzler.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.weezzler.android.R;
import com.weezzler.android.model.Album;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.server.ServerManager;

import java.util.ArrayList;
import java.util.List;

public class ArtistFragment extends Fragment {

    private AlbumListAdapter mAlbumListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_artist, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mAlbumListAdapter = new AlbumListAdapter(getActivity());

        TextView artistName = (TextView) view.findViewById(R.id.artist_name);
        artistName.setText(MediaLibrary.selectedArtist.getName());

        TextView albumCount = (TextView) view.findViewById(R.id.album_count);
        albumCount.setText(MediaLibrary.selectedArtist.getSubTitle(false));

        Button mPlayArtistButton = (Button) view.findViewById(R.id.play_all_button);
        mPlayArtistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServerManager.sendPlayArtist(MediaLibrary.selectedArtist);
            }
        });

        Button mShuffleArtistButton = (Button) view.findViewById(R.id.shuffle_all_button);
        mShuffleArtistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServerManager.sendShuffleArtist(MediaLibrary.selectedArtist);
            }
        });

        ListView listView = (ListView) view.findViewById(R.id.albums_list);
        listView.setAdapter(mAlbumListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LibraryItem item = mAlbumListAdapter.getItem(position);

                MediaLibrary.selectedAlbum = (Album)item;
                initFragment(new AlbumFragment());
            }
        });

        List<Album> albums = MediaLibrary.selectedArtist.getAlbums();

        if (albums != null && albums.size() > 0) {
            mAlbumListAdapter.clear();
            for (LibraryItem item : albums) {
                mAlbumListAdapter.add(item);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.options_menu, menu);

        MenuItem mPlayQueueMenuItem = menu.findItem(R.id.action_queue);
        mPlayQueueMenuItem.setVisible(true);

        MenuItem mVolumeMenuItem = menu.findItem(R.id.action_volume);
        mVolumeMenuItem.setVisible(true);
    }

    private void initFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.library_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        AppCompatActivity currentActivity = (AppCompatActivity)getActivity();
        if (currentActivity != null) {
            ActionBar actionBar = currentActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private static class AlbumListAdapter extends ArrayAdapter<LibraryItem> {

        public AlbumListAdapter(Activity activity) {
            super(activity, R.layout.library_item, new ArrayList<LibraryItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LibraryItem item = getItem(position);
            return LibraryItemViewHolder.setupView((Activity) getContext(), convertView, parent, item, false, true, position);
        }
    }
}
