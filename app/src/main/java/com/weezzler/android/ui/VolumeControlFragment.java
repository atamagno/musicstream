package com.weezzler.android.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

import com.weezzler.android.R;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.server.ServerManager;

public class VolumeControlFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View volumeControl = inflater.inflate(R.layout.volume_control, null);
        SeekBar volumeSeekBar = (SeekBar)volumeControl.findViewById(R.id.volumeSeekBar);
        volumeSeekBar.setProgress(MediaLibrary.volume);

        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ServerManager.sendVolume(seekBar.getProgress());
            }
        });

        builder.setView(volumeControl).setMessage(R.string.volume_title)
                .setPositiveButton(R.string.close_volume, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }
}
