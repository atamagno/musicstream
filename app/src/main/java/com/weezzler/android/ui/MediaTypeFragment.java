package com.weezzler.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;

import com.weezzler.android.R;
import com.weezzler.android.model.Artist;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;

import java.util.ArrayList;
import java.util.List;

public class MediaTypeFragment extends Fragment {

    public static final int ARTISTS_VIEW = 0;
    public static final int ALBUMS_VIEW = 1;
    public static final int SONGS_VIEW = 2;

    private static final String ARTISTS_VIEW_TAG = "com.weezzler.android.ui.ArtistsView";
    private static final String ALBUMS_VIEW_TAG = "com.weezzler.android.ui.AlbumsView";
    private static final String SONGS_VIEW_TAG = "com.weezzler.android.ui.SongsView";

    private LibraryFragmentListener mLibraryFragmentListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_library, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mMediaTypePagerAdapter = new MediaTypePagerAdapter();
        mViewPager.setAdapter(mMediaTypePagerAdapter);
        mViewPager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {

                        position = MediaLibrary.isVideoSelected() ? position + 1 : position;

                        switch (position) {
                            case ARTISTS_VIEW:
                                MediaLibrary.currentView = ARTISTS_VIEW;
                                break;
                            case ALBUMS_VIEW:
                                MediaLibrary.currentView = ALBUMS_VIEW;
                                break;
                            case SONGS_VIEW:
                                MediaLibrary.currentView = SONGS_VIEW;
                                break;
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private ViewPager mViewPager;
    private MediaTypePagerAdapter mMediaTypePagerAdapter;
    private SlidingTabLayout mSlidingTabLayout;
    public void initViewPager() {
        mMediaTypePagerAdapter = new MediaTypePagerAdapter();
        mViewPager.setAdapter(mMediaTypePagerAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLibraryFragmentListener = (LibraryFragmentListener) activity;
    }

    public static class MediaTypeAdapter extends ArrayAdapter<LibraryItem> implements Filterable {

        public MediaTypeAdapter(Activity context) {
            super(context, R.layout.library_item, new ArrayList<LibraryItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LibraryItem item = getItem(position);
            return LibraryItemViewHolder.setupView((Activity) getContext(), convertView, parent, item, false, false, position);
        }

        private List<? extends LibraryItem> originalData = null;
        public List<? extends LibraryItem> filteredData = null;
        private final ItemFilter mFilter = new ItemFilter();

        public Filter getFilter() {
            return mFilter;
        }

        private class ItemFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                List<? extends LibraryItem> list = null;
                switch (MediaLibrary.currentView) {
                    case MediaTypeFragment.ARTISTS_VIEW:
                        list = MediaLibrary.getArtists();
                        break;
                    case MediaTypeFragment.ALBUMS_VIEW:
                        list = MediaLibrary.getAlbums();
                        break;
                    case MediaTypeFragment.SONGS_VIEW:
                        list = MediaLibrary.getPlayableItems();
                        break;
                }

                int count = list != null ? list.size() : 0;
                final ArrayList<LibraryItem> nlist = new ArrayList<>(count);

                LibraryItem filterableLibraryItem;

                for (int i = 0; i < count; i++) {
                    filterableLibraryItem = list.get(i);
                    if (filterableLibraryItem.getName().toLowerCase().contains(filterString)) {
                        nlist.add(filterableLibraryItem);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<Artist>) results.values;

                clear();
                for (LibraryItem item : filteredData) {
                    add(item);
                }

                notifyDataSetChanged();
            }
        }
    }

    public class MediaTypePagerAdapter extends PagerAdapter {

        public int getCount() {
            return MediaLibrary.isVideoSelected() ? 2 : 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            position = MediaLibrary.isVideoSelected() ? position + 1 : position;

            switch (position) {
                case ARTISTS_VIEW:
                    return getString(R.string.artist_title_plural);
                case ALBUMS_VIEW:
                    return MediaLibrary.isVideoSelected() ? getString(R.string.folders_title_plural) : getString(R.string.album_title_plural);
                case SONGS_VIEW:
                    return MediaLibrary.isVideoSelected() ? getString(R.string.videos_title_plural) : getString(R.string.song_title_plural);
            }

            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            position = MediaLibrary.isVideoSelected() ? position + 1 : position;

            View view = null;
            switch (position) {
                case ARTISTS_VIEW:
                    view = getActivity().getLayoutInflater().inflate(R.layout.all_artists_list, container, false);
                    view.setTag(ARTISTS_VIEW_TAG);
                    break;
                case ALBUMS_VIEW:
                    view = getActivity().getLayoutInflater().inflate(R.layout.all_albums_list, container, false);
                    view.setTag(ALBUMS_VIEW_TAG);
                    break;
                case SONGS_VIEW:
                    view = getActivity().getLayoutInflater().inflate(R.layout.all_songs_list, container, false);
                    view.setTag(SONGS_VIEW_TAG);
                    break;
            }

            container.addView(view);

            populateMediaTypeList(view, position);

            return view;
        }

        private void populateMediaTypeList(View view, int position) {

            MediaTypeAdapter mMediaTypeAdapter = new MediaTypeAdapter(getActivity());

            List<? extends LibraryItem> libraryItems = null;

            switch (position) {
                case ARTISTS_VIEW:
                    libraryItems = MediaLibrary.getArtists();
                    break;
                case ALBUMS_VIEW:
                    libraryItems = MediaLibrary.getAlbums();
                    break;
                case SONGS_VIEW:
                    libraryItems = MediaLibrary.getPlayableItems();
                    break;
            }

            if (libraryItems != null && libraryItems.size() > 0) {
                mMediaTypeAdapter.clear();
                for (LibraryItem item : libraryItems) {
                    mMediaTypeAdapter.add(item);
                }
                mMediaTypeAdapter.notifyDataSetChanged();
            }

            final ListView listView = (ListView) view.findViewById(R.id.list_view);
            listView.setAdapter(mMediaTypeAdapter);
            listView.setTextFilterEnabled(true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
                    LibraryItem item = (LibraryItem) listView.getItemAtPosition(itemPosition);
                    item.setListPosition(itemPosition);
                    mLibraryFragmentListener.onMediaItemSelected(item);
                }
            });
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public interface LibraryFragmentListener {
        void onMediaItemSelected(LibraryItem item);
    }
}
