package com.weezzler.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.weezzler.android.LibraryActivity;
import com.weezzler.android.R;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;
import com.weezzler.android.server.ServerManager;

import java.util.ArrayList;
import java.util.List;

public class AlbumFragment extends Fragment {

    private SongListAdapter mSongListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_album, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mSongListAdapter = new SongListAdapter(getActivity());

        if (MediaLibrary.selectedAlbum != null) {

            TextView albumName = (TextView) view.findViewById(R.id.album_name);
            albumName.setText(MediaLibrary.selectedAlbum.getName());

            TextView songCount = (TextView) view.findViewById(R.id.song_count);
            songCount.setText(MediaLibrary.selectedAlbum.getSubTitle(false));

            Button mPlayArtistButton = (Button) view.findViewById(R.id.play_all_button);
            mPlayArtistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerManager.sendPlayAlbum(MediaLibrary.selectedAlbum);
                }
            });

            Button mShuffleArtistButton = (Button) view.findViewById(R.id.shuffle_all_button);
            mShuffleArtistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerManager.sendShuffleAlbum(MediaLibrary.selectedAlbum);
                }
            });

            ListView listView = (ListView) view.findViewById(R.id.songs_list);
            listView.setAdapter(mSongListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    LibraryItem item = mSongListAdapter.getItem(position);
                    item.setListPosition(position);
                    ((LibraryActivity)getActivity()).playSong(item, "album");
                }
            });

            List<PlayableItem> playableItems = MediaLibrary.selectedAlbum.getPlayableItems();

            if (playableItems != null && playableItems.size() > 0) {
                mSongListAdapter.clear();
                for (LibraryItem item : playableItems) {
                    mSongListAdapter.add(item);
                }
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.options_menu, menu);

        MenuItem mPlayQueueMenuItem = menu.findItem(R.id.action_queue);
        mPlayQueueMenuItem.setVisible(true);

        MenuItem mVolumeMenuItem = menu.findItem(R.id.action_volume);
        mVolumeMenuItem.setVisible(true);
    }

    private static class SongListAdapter extends ArrayAdapter<LibraryItem> {

        public SongListAdapter(Activity context) {
            super(context, R.layout.library_item, new ArrayList<LibraryItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LibraryItem item = getItem(position);
            return LibraryItemViewHolder.setupView((Activity) getContext(), convertView, parent, item, false, false, position);
        }
    }
}
