package com.weezzler.android.ui;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.weezzler.android.R;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;

import java.util.ArrayList;
import java.util.List;

public class SearchResultAdapter extends ArrayAdapter<LibraryItem> implements Filterable {

    public SearchResultAdapter(Activity context) {
        super(context, R.layout.library_item, new ArrayList<LibraryItem>());

        switch (MediaLibrary.currentView) {
            case MediaTypeFragment.ARTISTS_VIEW:
                originalData = MediaLibrary.getArtists();
                break;
            case MediaTypeFragment.ALBUMS_VIEW:
                originalData = MediaLibrary.getAlbums();
                break;
            case MediaTypeFragment.SONGS_VIEW:
                originalData = MediaLibrary.getPlayableItems();
                break;
        }

        if (originalData != null && originalData.size() > 0) {
            clear();
            for (LibraryItem item : originalData) {
                add(item);
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LibraryItem item = getItem(position);
        return LibraryItemViewHolder.setupView((Activity) getContext(), convertView, parent, item, false, false, position);
    }

    private static List<? extends LibraryItem> originalData = null;
    private List<? extends LibraryItem> filteredData = null;
    private final ItemFilter mFilter = new ItemFilter();

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            int count = originalData.size();
            final ArrayList<LibraryItem> nlist = new ArrayList<>(count);

            LibraryItem filterableLibraryItem;

            for (int i = 0; i < count; i++) {
                filterableLibraryItem = originalData.get(i);
                if (filterableLibraryItem.getName().toLowerCase().contains(filterString)) {
                    nlist.add(filterableLibraryItem);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<LibraryItem>) results.values;

            clear();
            for (LibraryItem item : filteredData) {
                add(item);
            }

            notifyDataSetChanged();
        }
    }
}
