package com.weezzler.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.weezzler.android.LibraryActivity;
import com.weezzler.android.R;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.server.ServerManager;

import java.util.ArrayList;

public class PlayQueueFragment extends Fragment {

    private PlayQueueAdapter mPlayQueueAdapter;
    private TextView mTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_play_queue, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mPlayQueueAdapter = new PlayQueueAdapter(getActivity());

        mTextView = (TextView) view.findViewById(R.id.text_play_queue);
        mTextView.setText(getString(R.string.empty_play_queue));

        TextView mPlayQueueTitle = (TextView) view.findViewById(R.id.play_queue_title);
        mPlayQueueTitle.setText(getString(R.string.play_queue_title));

        Button mClearPlayQueue = (Button) view.findViewById(R.id.clear_button);
        mClearPlayQueue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServerManager.sendClearPlayQueue();
            }
        });

        ListView mListView = (ListView) view.findViewById(R.id.play_queue_songs_list);
        mListView.setAdapter(mPlayQueueAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                LibraryItem item = mPlayQueueAdapter.getItem(position);
                item.setListPosition(position);
                MediaLibrary.playingPosition = position;
                ((LibraryActivity)getActivity()).playSong(item, "playQueue");
            }
        });

        updatePlayQueue();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.options_menu, menu);

        MenuItem mVolumeMenuItem = menu.findItem(R.id.action_volume);
        mVolumeMenuItem.setVisible(true);
    }

    public void updatePlayQueue() {

        if (mPlayQueueAdapter != null) {

            if (MediaLibrary.playQueue != null) {

                mPlayQueueAdapter.clear();

                if (MediaLibrary.playQueue.size() > 0) {
                    for (LibraryItem item : MediaLibrary.playQueue) {
                        mPlayQueueAdapter.add(item);
                    }
                    mTextView.setVisibility(View.GONE);
                } else {
                    mTextView.setVisibility(View.VISIBLE);
                }

                mPlayQueueAdapter.notifyDataSetChanged();

            } else {
                mTextView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void updateSelectedSong() {
        if (mPlayQueueAdapter != null) {
            mPlayQueueAdapter.notifyDataSetChanged();
        }
    }

    private static class PlayQueueAdapter extends ArrayAdapter<LibraryItem> {

        public PlayQueueAdapter(Activity activity) {
            super(activity, R.layout.library_item, new ArrayList<LibraryItem>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LibraryItem item = getItem(position);
            return LibraryItemViewHolder.setupView((Activity) getContext(), convertView, parent, item, true, false, position);
        }
    }
}
