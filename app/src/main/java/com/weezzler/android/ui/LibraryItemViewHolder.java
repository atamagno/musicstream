package com.weezzler.android.ui;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.weezzler.android.R;
import com.weezzler.android.model.LibraryItem;
import com.weezzler.android.model.MediaLibrary;
import com.weezzler.android.model.PlayableItem;
import com.weezzler.android.server.ServerManager;

class LibraryItemViewHolder {

    private TextView mTitleView;
    private TextView mDescriptionView;
    private ImageButton mAddToQueueButton;
    private ImageView mVideoThumbnail;

    public static View setupView(Activity activity, View convertView, ViewGroup parent, LibraryItem item, boolean isPlayQueueItem, boolean isArtistView, int position) {

        LibraryItemViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.library_item, parent, false);
            holder = new LibraryItemViewHolder();
            holder.mTitleView = (TextView) convertView.findViewById(R.id.title);
            holder.mDescriptionView = (TextView) convertView.findViewById(R.id.description);
            holder.mAddToQueueButton = (ImageButton) convertView.findViewById(R.id.add_to_queue);
            holder.mVideoThumbnail = (ImageView) convertView.findViewById(R.id.video_thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (LibraryItemViewHolder) convertView.getTag();
        }

        holder.mTitleView.setText(item.getName());
        holder.mDescriptionView.setText(item.getSubTitle(isArtistView));

        if (item.getType() == LibraryItem.MediaType.SONG) {
            holder.mVideoThumbnail.setImageBitmap(((PlayableItem)item).getBitmap());
        }

        if (!isPlayQueueItem) {
            final LibraryItem mediaItem = item;
            holder.mAddToQueueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String toastText = "";
                    switch (mediaItem.getType()) {
                        case ARTIST:
                            toastText = view.getContext().getString(R.string.artist_add_to_queue);
                            break;
                        case ALBUM:
                            int resIdAlbum = MediaLibrary.isVideoSelected() ? R.string.folder_add_to_queue : R.string.album_add_to_queue;
                            toastText = view.getContext().getString(resIdAlbum);
                            break;
                        case SONG:
                            int resIdPlayableItem = MediaLibrary.isVideoSelected() ? R.string.video_add_to_queue : R.string.song_add_to_queue;
                            toastText = view.getContext().getString(resIdPlayableItem);
                            break;
                    }

                    ServerManager.addToPlayList(mediaItem);

                    Toast toast = Toast.makeText(view.getContext(), toastText, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 140);
                    toast.show();
                }
            });
        } else {

            boolean isPlaying = ((PlayableItem)item).IsPlaying();
            if (isPlaying && (position == MediaLibrary.playingPosition)) {
                holder.mTitleView.setTypeface(null, Typeface.BOLD);
            } else {
                holder.mTitleView.setTypeface(null, Typeface.NORMAL);
            }
            holder.mAddToQueueButton.setVisibility(View.GONE);
        }

        return convertView;
    }
}
